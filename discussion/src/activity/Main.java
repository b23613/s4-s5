package com.zuitt.activity;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

        Contact firstContact = new Contact();
        Contact secondContact = new Contact();
        Contact thirdContact = new Contact();

        firstContact.setName("John Doe");
        firstContact.setContactNumber("+639991234567");
        firstContact.setAddress("Las Piñas City");

        secondContact.setName("Jane");
        secondContact.setContactNumber("+639297654321");
        secondContact.setAddress("Taguig City");

        thirdContact.setName("Jay");
        thirdContact.setContactNumber("+639295118900");
        thirdContact.setAddress("Parañaque City");

        Phonebook phonebook = new Phonebook();

        phonebook.addContact(firstContact);
        phonebook.addContact(secondContact);
        phonebook.addContact(thirdContact);

        if(phonebook.getContact().size() == 0){
            System.out.println("Phonebook is Empty!");
        }
        else{
            ArrayList<Contact> contacts = phonebook.getContact();

            for(Contact contact : contacts){
                contact.printContact();
            }


        }



    }
}

